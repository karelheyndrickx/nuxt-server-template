const pkg = require('./package')


module.exports = {
  mode: 'universal',

  env: {
    //TODO: Replace with external api url
    baseUrl: 'http://localhost:56461/api/' || 'https://api.kanye.rest/'
  },

  /*
  ** Headers of the page
  */
  head: {
    //TODO: Set the meta data with the actual data of the application.
    titleTemplate: "Applicationname - %s",
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: pkg.description }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }
    ]
  },

  /*
  ** Customize the progress-bar color
  */
  loading: { color: '#fff' },

  /*
  ** Global CSS
  */
  css: [
  ],

  /*
  ** Plugins to load before mounting the App
  */
  plugins: [
  ],

  /*
  ** Nuxt.js modules
  */
  modules: [
    // Doc: https://axios.nuxtjs.org/usage
    '@nuxtjs/axios',

    ['nuxt-fontawesome', {
      component: 'fa',
      imports: [
        //TODO: Type the icons you want to import for each set
        {
          set: '@fortawesome/free-solid-svg-icons',
          icons: ['faDragon']
          // can be called like so  <fa :icon="['fas', 'dragon']" />
        },
        {
          set: '@fortawesome/pro-regular-svg-icons',
          icons: ['faDollarSign']
          // can be called like so  <fa :icon="['far', 'dollar-sign']" />
        }
        ,
        {
          set: '@fortawesome/pro-light-svg-icons',
          icons: ['faAxeBattle']
          // can be called like so  <fa :icon="['fal', 'axe-battle']" />
        }
        ,
        {
          set: '@fortawesome/pro-solid-svg-icons',
          icons: ['faUnicorn']
          // can be called like so  <fa :icon="['fas', 'unicorn']" />
        }
      ]
    }]
  ],
  /*
  ** Axios module configuration
  */
  axios: {
    // See https://github.com/nuxt-community/axios-module#options
  },

  /*
  ** Build configuration
  */
  build: {
    /*
    ** You can extend webpack config here
    */
    extend(config, ctx) {

    }
  }
}
