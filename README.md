# Nuxt Server Template

This repository features a basic Nuxt template in which Sass, Fontawesome and Axios are already installed. It has a standard index.vue page and 1 component to demonstrate using those. It also shows how you can use a font-awesome icon and can import a component dynamically

*This template is useful for headless purposes. The content for the site is accessible via a web-service API, usually in a RESTful manner and in a mashup-friendly format such as JSON*

## What is installed?

- Sass
- Fontawesome + pro version
- Axios

## How do I make it work?

``` bash
# install dependencies
$ npm install

# serve with hot reload at localhost:3000
$ npm run dev
```

For detailed explanation on how things work, checkout [Nuxt.js docs](https://nuxtjs.org).

## What should I change?

These instructions will help you on your way with the template.

### Step 1 - package.json

Change all the needed properties.

### Step 2 - nuxt.config.js

First, change the value of baseURL to the url that you'll be using in your project. If you're feeling frisky you can have a look at what `https://api.kanye.rest` will bring you!

```javascript
  env: {
    //TODO: Replace with external api url
    baseUrl: 'http://localhost:56461/api/' || 'https://api.kanye.rest/'
  },
```

Second, set the meta data with the actual data of the application. 

```javascript
  head: {
     //TODO: Set the meta data with the actual data of the application.
    titleTemplate: "Applicationname - %s",
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: pkg.description }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }
    ]
  },
```

> Note: in the titleTemplate %s will be replaced with the given title in a page.

Third, import the font-awesome icons of your choosing. 

```javascript
modules: [
    // Doc: https://axios.nuxtjs.org/usage
    '@nuxtjs/axios',

    ['nuxt-fontawesome', {
      component: 'fa',
      imports: [
        //TODO: Type the icons you want to import for each set
        {
          set: '@fortawesome/free-solid-svg-icons',
          icons: ['faDragon']
          // can be called like so  <fa :icon="['fas', 'dragon']" />
        },
        {
          set: '@fortawesome/pro-regular-svg-icons',
          icons: ['faDollarSign']
          // can be called like so  <fa :icon="['far', 'dollar-sign']" />
        }
        ,
        {
          set: '@fortawesome/pro-light-svg-icons',
          icons: ['faAxeBattle']
          // can be called like so  <fa :icon="['fal', 'axe-battle']" />
        }
        ,
        {
          set: '@fortawesome/pro-solid-svg-icons',
          icons: ['faUnicorn']
          // can be called like so  <fa :icon="['fas', 'unicorn']" />
        }
      ]
    }]
  ],
```

### Step 3 - /pages & /components

These vue components can entirely be deleted as long as your new components conclude the following code

```javascript
export default {
    head() {
    return {
      title: "Start page",
      meta: [
        // hid is used as unique identifier. Do not use `vmid` for it as it will not work
        {
          hid: "description",
          name: "description",
          content: "This is the start page of the nuxt server"
        }
      ]
    };
  }
}
```

For more information about the implementation of **meta** tags, have a look at the following two links. 

- [The head property](https://nuxtjs.org/api/configuration-head/) 
- [The head method](https://nuxtjs.org/api/pages-head/) 

### Step 4 - Done!

That's it, you're ready to go!

## Static generated application or Server Side Rendering? 

### Generation of static pages

Nuxt is able to generate HTML pages out of your application. By running the command 

```bash
$ npm run generate
```

 your nuxt server will build and generate plain old HTML files. These files will be put in the folder `dist` which you could upload to a server. 

Think about the loading speed of a webpage!  

For more information about the generation of static pages, have a look at [The generate Property](https://nuxtjs.org/api/configuration-generate/)

### Server Side Rendering

An other thing Nuxt can do is pre-render your pages so the loading speed will increase massively. This can be done with the command 

```bash
$ npm run build
```

 followed by

```bash
$ npm run start
```

Nuxt will parse all the pages and generate the HTML for each one before sending it to the server. Server side rendering removes the build process or the generation of the page on the client side and therefor makes it faster for the client to load a page. 

